<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

# Videos Pepito Back-End

El BackEnd de este proyecto esta desarrollados en PHP (Laravel framework v5.8) la autenticación se realiza con JWT y la documentación de la API se realizo con Swagger OAS3  

## Empezando

Estas instrucciones le brindarán una copia del proyecto en funcionamiento en su máquina local para fines de desarrollo y prueba.

### Prerequisites

leer Requisitos del servidor para Laravel 5.8
los más importantes son: 

Contar con:
```
PHP> = 7.1.3
Composer

```

### Installación

Luego de tener clonado el repositorio, es necesario ejecutar los siguientes pasasos:

##### Actualizar composer 

```
composer update
```

##### Generar el archivo de configuracion .env

```
copy .env.example .env
```

##### Generar la key de laravel 

```
php artisan key:generate
```

##### Correr migraciones

```
php artisan migrate --seed
```

### Generar la Documentación con Swagger

ejecutamos este comendo

```
php artisan l5-swagger:generate 
```

NOTA: configurar VirtualHost para que el acceso a este proyecto sea http://videospepitoback.loc

de esta forma la ruta para acceder a la documantación desde cualquier navegador es

* [Documentación](http://videospepitoback.loc/api/documentation) - http://videospepitoback.loc/api/documentation


