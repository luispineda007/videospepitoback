<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('users', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->string('documento');
      $table->string('nickname');
      $table->string('nombre');
      $table->string('telefono');
      $table->string('email')->unique();
      $table->enum("rol", [User::ROL_ADMIN, User::ROL_CLIENTE]);
      $table->timestamp('email_verified_at')->nullable();
      $table->string('password');
      $table->enum('estado',["Activo","Vetado"])->default("Activo");
      $table->rememberToken();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('users');
  }
}
