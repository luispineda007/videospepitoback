<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeliculaUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelicula_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('pelicula_id');
            $table->unsignedBigInteger('user_id');
            $table->dateTime('fecha_reserva');
            $table->dateTime('fecha_alquiler')->nullable();
            $table->dateTime('fecha_entrega')->nullable();
            $table->enum("estado", ["Reservada","Alquilada","Entregada","Reserva-Cancelada"]);
            $table->timestamps();

          $table->foreign('pelicula_id')
            ->references('id')
            ->on('peliculas')
            ->onDelete('cascade');

          $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelicula_user');
    }
}
