<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeliculasTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('peliculas', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->longText('portada');
      $table->string('nombre');
      $table->text('descripcion');
      $table->string('genero');
      $table->enum('cartelera',["X",""])->default("X");
      $table->string('fecha_lanzamiento');
      $table->string('costo');
      $table->string('cantidad');
      $table->enum('estado',["disponible","descontinuada"])->default("disponible");
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('peliculas');
  }
}
