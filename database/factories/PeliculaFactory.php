<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */


use App\Pelicula;
use Faker\Generator as Faker;

$factory->define(Pelicula::class, function (Faker $faker) {
    return [
        'genero' => $faker->randomElement(["comedia","accion","terror","infantil","suspenso"]),
        'fecha_lanzamiento' => $faker->numberBetween(1994,2019),
        'cartelera' => $faker->randomElement(["X",""]),
        'costo'=> $faker->numberBetween(10,70)*100,
        'cantidad' => $faker->numberBetween(1,11),
    ];
});
