<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    User::create([
      'email' => 'admin@gmail.com',
      'password' => Hash::make('123456'),
      'nickname' => 'luispinda',
      'nombre' => "Luis Carlos Pineda",
      'documento' => '123456789',
      'telefono' => '3138585565',
      'rol' => User::ROL_ADMIN,
    ]);

    factory(User::class, 10)->create(["rol"=>User::ROL_CLIENTE]);


  }
}
