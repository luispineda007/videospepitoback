<?php

use App\Pelicula;
use Illuminate\Database\Seeder;

class PeliculasTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {

    factory(Pelicula::class)->create([
      "nombre" => "La vida es bella",
      "descripcion" => "un joven llamado Guido llega a una ciudad de la Toscana (Arezzo) con la intención de abrir una librería. Allí conoce a Dora y, a pesar de que es la prometida del fascista Ferruccio, se casa con ella y tiene un hijo... ",
      "portada" => "lavidaesbella.jpg",
    ]);

    factory(Pelicula::class)->create([
      "nombre" => "Forrest Gump",
      "descripcion" => "Forrest Gump (Tom Hanks) es un chico que sufre un cierto retraso mental. A pesar de todo, gracias a su tenacidad y a su buen corazón será protagonista de acontecimientos cruciales de su país. Jenny, su gran amor desde la infancia, será la persona más importante de su vida.",
      "portada" => "ForrestGump.jpg",
    ]);

    factory(Pelicula::class)->create([
      "nombre" => "El rey león",
      "descripcion" => "La sabana africana es el escenario en el que tienen lugar las aventuras de Simba, un pequeño león que es el heredero del trono. Sin embargo, al ser injustamente acusado por el malvado Scar de la muerte de su padre, se ve obligado a exiliarse. Durante su...",
      "portada" => "Elreyleon.jpg",
    ]);

    factory(Pelicula::class)->create([
      "nombre" => "Titanic",
      "descripcion" => "JAMES CAMERON, 1997 Jack (DiCaprio), un joven artista, gana en una partida de cartas un pasaje para viajar a América en el Titanic, el trasatlántico más grande y seguro jamás construido. A bordo conoce a Rose (Kate Winslet), una joven de una buena familia venida a menos que va a contraer un...",
      "portada" => "Titanic.jpg",
    ]);

    factory(Pelicula::class)->create([
      "nombre" => "Eduardo Manostijeras",
      "descripcion" => "Durante una noche de Navidad, una anciana le cuenta a su nieta la historia de Eduardo Manostijeras (Johnny Depp), un muchacho creado por un extravagante inventor (Vincent Price) que no pudo acabar su obra, dejando al joven con cuchillas en lugar de dedos.",
      "portada" => "eduardomanostijeras.jpg",
    ]);

    factory(Pelicula::class)->create([
      "nombre" => "Up",
      "descripcion" => "PETE DOCTER Y BOB PETERSON, 2009 Carl Fredricksen es un viudo vendedor de globos de 78 años que, finalmente, consigue llevar a cabo el sueño de su vida: enganchar miles de globos a su casa y salir volando rumbo a América del Sur. Pero ya estando en el aire y sin posibilidad de retornar Carl... Ver mas",
      "portada" => "up.jpg",
    ]);

    factory(Pelicula::class)->create([
      "nombre" => "Gladiador",
      "descripcion" => "RIDLEY SCOTT, 2000 En el año 180, el Imperio Romano domina todo el mundo conocido. Tras la victoria sobre los bárbaros, el anciano emperador Marco Aurelio (Richard Harris) decide transferir el poder a Máximo (Russell Crowe), bravo general de sus ejércitos y hombre de inquebrantable lealtad al..",
      "portada" => "gladiador.jpg",
    ]);

    factory(Pelicula::class)->create([
      "nombre" => "Monstruos S. A.",
      "descripcion" => "PETE DOCTER, LEE UNKRICH Y DAVID SILVERMAN, 2001 Monsters Inc. es la mayor empresa de miedo del mundo, y James P. Sullivan es uno de sus mejores empleados. Asustar a los niños no es un trabajo fácil, ya que todos creen que los niños son tóxicos y no pueden tener contacto con ellos. Pero un día... ",
      "portada" => "monstruossa.jpg",
    ]);
    factory(Pelicula::class)->create([
      "nombre" => "La lista de Schindler",
      "descripcion" => "STEVEN SPIELBERG, 1993 Segunda Guerra Mundial (1939-1945). Oskar Schindler (Liam Neeson), un hombre de enorme astucia y talento para las relaciones públicas, organiza un ambicioso plan para ganarse la simpatía de los nazis. Después de la invasión de Polonia por los alemanes (1939), consigue... Ver mas",
      "portada" => "lalistadeschindler.jpg",
    ]);
    factory(Pelicula::class)->create([
      "nombre" => "Toy Story",
      "descripcion" => "JOHN LASSETER, 1995 Los juguetes de Andy, un niño de 6 años, temen que haya llegado su hora y que un nuevo regalo de cumpleaños les sustituya en el corazón de su dueño. Woody, un vaquero que ha sido hasta ahora el juguete favorito de Andy, trata de tranquilizarlos hasta que aparece Buzz... ",
      "portada" => "toystory.jpg",
    ]);
    factory(Pelicula::class)->create([
      "nombre" => "El pianista",
      "descripcion" => "ROMAN POLANSKI, 2002 Wladyslaw Szpilman, un brillante pianista polaco de origen judío, vive con su familia en el ghetto de Varsovia. Cuando, en 1939, los alemanes invaden Polonia, consigue evitar la deportación gracias a la ayuda de algunos amigos. Pero tendrá que vivir escondido y...",
      "portada" => "elpianista.jpg",
    ]);
    factory(Pelicula::class)->create([
      "nombre" => "Shrek",
      "descripcion" => "ANDREW ADAMSON Y VICKY JENSON, 2001 Hace mucho tiempo, en una lejanísima ciénaga, vivía un feroz ogro llamado Shrek. De repente, un día, su soledad se ve interrumpida por una invasión de sorprendentes personajes. Hay ratoncitos ciegos en su comida, un enorme y malísimo lobo en su cama, tres...",
      "portada" => "shrek.jpg",
    ]);
    factory(Pelicula::class)->create([
      "nombre" => "El Señor de los anillos: El retorno del rey",
      "descripcion" => "PETER JACKSON, 2003 Las fuerzas de Saruman han sido destruidas, y su fortaleza sitiada. Ha llegado el momento de decidir el destino de la Tierra Media, y, por primera vez, parece que hay una pequeña esperanza. El interés del señor oscuro Sauron se centra ahora en Gondor, el último reducto de... Ver mas",
      "portada" => "elsenordelosanillosrey.jpg",
    ]);
    factory(Pelicula::class)->create([
      "nombre" => "Avatar",
      "descripcion" => "JAMES CAMERON, 2009 Año 2154. Jake Sully (Sam Worthington), un ex-marine condenado a vivir en una silla de ruedas, sigue siendo, a pesar de ello, un auténtico guerrero. Precisamente por ello ha sido designado para ir a Pandora, donde algunas empresas están extrayendo un mineral extraño que...",
      "portada" => "avatar.jpg",
    ]);
    factory(Pelicula::class)->create([
      "nombre" => "Cadena perpetua",
      "descripcion" => "FRANK DARABONT, 1994 Acusado del asesinato de su mujer, Andrew Dufresne (Tim Robbins), tras ser condenado a cadena perpetua, es enviado a la cárcel de Shawshank. Con el paso de los años conseguirá ganarse la confianza del director del centro y el respeto de sus compañeros de prisión... ",
      "portada" => "cadenaperpetua.jpg",
    ]);

    factory(Pelicula::class)->create([
      "nombre" => "Piratas del Caribe: La maldición de la Perla Negra",
      "descripcion" => "GORE VERBINSKI, 2003 Mar Caribe, siglo XVIII. El aventurero capitán Jack Sparrow piratea en aguas caribeñas, pero su andanzas terminan cuando su enemigo, el Capitán Barbossa, después de robarle su barco, el Perla Negra, ataca la ciudad de Port Royal y secuestra a Elizabeth Swann, la hija del..",
      "portada" => "piratasdelcaribe.jpg",
    ]);

    factory(Pelicula::class)->create([
      "nombre" => "Regreso al futuro",
      "descripcion" => "ROBERT ZEMECKIS, 1985 El adolescente Marty McFly es amigo de Doc, un científico al que todos toman por loco. Cuando Doc crea una máquina para viajar en el tiempo, un error fortuito hace que Marty llegue a 1955, año en el que sus futuros padres aún no se habían conocido. Después de impedir su...",
      "portada" => "regresoalfuturo.jpg",
    ]);

    factory(Pelicula::class)->create([
      "nombre" => "Buscando a Nemo",
      "descripcion" => "ANDREW STANTON Y LEE UNKRICH, 2003 El pececillo Nemo, que es hijo único, es muy querido y protegido por su padre. Después de ser capturado en un arrecife australiano va a parar a la pecera de la oficina de un dentista de Sidney. Su tímido padre emprenderá una peligrosa aventura para rescatarlo...",
      "portada" => "buscandoanemo.jpg",
    ]);

    factory(Pelicula::class)->create([
      "nombre" => "Braveheart",
      "descripcion" => "MEL GIBSON, 1995 En el siglo XIV, los escoceses viven oprimidos por los gravosos tributos y las injustas leyes impuestas por los ingleses. William Wallace es un joven escocés que regresa a su tierra despues de muchos años de ausencia. Siendo un niño, toda su familia fue asesinada por los...",
      "portada" => "braveheart.jpg",
    ]);

    factory(Pelicula::class)->create([
      "nombre" => "El caballero oscuro",
      "descripcion" => "CHRISTOPHER NOLAN, 2008 Batman (Christian Bale) regresa para continuar su guerra contra el crimen. Con la ayuda del teniente Jim Gordon (Gary Oldman) y del Fiscal del Distrito Harvey Dent (Aaron Eckhart), Batman se propone destruir el crimen organizado en la ciudad de Gotham. El triunvirato...",
      "portada" => "elcaballerooscuro.jpg",
    ]);
  }
}
