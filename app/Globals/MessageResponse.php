<?php
	/**
	 * Created by PhpStorm.
	 * User: ceindetecpc
	 * Date: 24/04/18
	 * Time: 11:42 AM
	 */

	namespace App\Globals;

	class MessageResponse {

		/**
		 * @global String constante para el login con exitosa
		 */
		const MESSAGE_LOGIN_SUCCESS = "Usuario logueado con exito";
		/**
		 * @global String constante para el login con fallido
		 */
		const MESSAGE_LOGIN_FAIL = "Usuario o contraseña invalido";
		/**
		 * @global String constante para el login con exitosa
		 */
		const MESSAGE_LOGIN_ERROR = "Ocurrio un error durante la utentificación";
		/**
		 * @global String constante para el mensaje de consulta exitosa
		 */
		const MESSAGE_QUERY_SUCCESS = "La busqueda produjo resultados";
		/**
		 * @global String constante para el mensaje de consulta vacia
		 */
		const MESSAGE_QUERY_EMPTY = "La busqueda NO produjo resultados";
		/**
		 * @global String constante para el mensaje de consulta error
		 */
		const MESSAGE_QUERY_ERROR = "La busqueda presenta errores";
		/**
		 * @global String constante para el mensaje de actualizacion exitoso
		 */
		const MESSAGE_UPDATE_SUCCESS = "La actualizacion fue existosa";
		/**
		 * @global String constante para el mensaje de actualizacion con error
		 */
		const MESSAGE_UPDATE_ERROR = "No fue posible realizar la actualizacion";
		/**
		 * @global String constante para el mensaje de creacion exitosa
		 */
		const MESSAGE_CREATE_SUCCESS = "El registro fue creado";
		/**
		 * @global String constante para el mensaje de crear con error
		 */
		const MESSAGE_CREATE_ERROR = "No fue posible crear el registro";
		/**
		 * @global String constante para el mensaje de creacion exitosa
		 */
		const MESSAGE_DELETE_SUCCESS = "El registro fue eliminado";
		/**
		 * @global String constante para el mensaje de crear con error
		 */
		const MESSAGE_DELETE_ERROR = "No fue posible eliminar el registro";
	}