<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model {

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nombre',
    'descripcion',
    'genero',
    'cartelera',
    'fecha_lanzamiento',
    'costo',
    'cantidad',
  ];

  public function scopeNombre($query, $nombre) {
    return $query->where('nombre',"like", "%".$nombre."%");
  }

  public function scopeGenero($query, $genero) {
    return $query->where('genero',"=", $genero);
  }

  public function users(){
    return $this->belongsToMany(User::class)
      ->withPivot('fecha_reserva','fecha_alquiler','fecha_entrega','estado');
  }

  public function reservas (){
    return $this->hasMany(Alquiler::class);
  }

}
