<?php

namespace App\Http\Controllers\API;

use App\Globals\CodesResponse;
use App\Globals\CryptoJs;
use App\Globals\MessageResponse;
use App\Globals\Utils;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\DataTables;

class UserController extends Controller {

  /**
   * @OA\Get(
   *     path="/api/users",
   *     tags={"Usuarios"},
   *     summary="Mostrar todos los usuarios del sistema",
   *     security={ {"bearer": {}} },
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="default",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function index() {

    $user = User::all();

    return Utils::responseSuccess("Todos los usuarios del sistema", CodesResponse::CODE_OK, $user);

  }


  /**
   * @OA\Get(
   *     path="/api/usersall",
   *     tags={"Usuarios"},
   *     summary="Mostrar todos los usuarios del sistema por medio de Datatable",
   *     security={ {"bearer": {}} },
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="default",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function users(Request $request) {

    $users = User::select(["id", "documento", "nombre", "nickname", "telefono", "estado"]);

    return DataTables::of($users)
      ->addColumn('documento', function ($users) use ($request) {
        return CryptoJs::cryptoJsAesEncrypt($request->token, $users->documento);
      })
      ->addColumn('nombre', function ($users) use ($request) {
        return CryptoJs::cryptoJsAesEncrypt($request->token, $users->nombre);
      })
      ->addColumn('nickname', function ($users) use ($request) {
        return CryptoJs::cryptoJsAesEncrypt($request->token, $users->nickname);
      })
      ->addColumn('telefono', function ($users) use ($request) {
        return CryptoJs::cryptoJsAesEncrypt($request->token, $users->telefono);
      })
      ->addColumn('action', function ($users) {
        $action = '<div class="btn-group">';
        if ($users->estado == User::ESTADO_ACTIVO) {
          $action .= '<button data-toggle="tooltip" title="Eliminar" class="btn btn-danger btn-xs" onclick="cambiarestado(' . $users->id
            . ')">Vetar</button>';
        } else {
          $action .= '<button data-toggle="tooltip" title="Eliminar" class="btn btn-success btn-xs" onclick="cambiarestado(' . $users->id
            . ')">Activar</button>';
        }

        $action .= '</div>';

        return $action;
      })
      ->toJson();

  }

  /**
   * @OA\Post(
   *     path="/api/user/cambiarestado",
   *     tags={"Usuarios"},
   *     summary="Metodo que se encarga de cambiar el estado del usuario",
   *    @OA\Parameter(
   *          name="user_id",
   *          description="id del usuario a cambiar el estado",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *     security={ {"bearer": {}} },
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="400",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function cambiarEstado(Request $request) {

    $user = Auth::user();

    if ($user->rol != User::ROL_ADMIN) {

      return Utils::responseError("No tienes permiso para hacer esto", CodesResponse::CODE_UNAUTHORIZED, []);

    }

    DB::beginTransaction();
    try {

      $user = User::query()->find($request->user_id);

      if ($user->estado == User::ESTADO_ACTIVO) {
        $user->estado = User::ESTADO_VETADO;
      } else {
        $user->estado = User::ESTADO_ACTIVO;
      }
      $user->save();
      DB::commit();

      return Utils::responseSuccess(MessageResponse::MESSAGE_UPDATE_SUCCESS, CodesResponse::CODE_OK, $user);

    } catch (\Exception $exception) {

      DB::rollBack();

      return Utils::responseError($exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);

    }

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create() {
    //
  }

  /**
   * @OA\Post(
   *     path="/api/user/store",
   *     tags={"Usuarios"},
   *     summary="Metodo que se encarga de registrar usuarios con el rol de Cliente",
   *    @OA\Parameter(
   *          name="nickname",
   *          description="Nombre de usuario Ejeplo pepito03",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *   @OA\Parameter(
   *          name="nombre",
   *          description="Nombre y apelidos del usuario",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *     @OA\Parameter(
   *          name="documento",
   *          description="Número del documento de identidad del usuario (debe ser único)",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *     @OA\Parameter(
   *          name="telefono",
   *          description="Número telefonico de contacto",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *      @OA\Parameter(
   *          name="email",
   *          description="Correo electronico (debe ser único)",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *      @OA\Parameter(
   *          name="password",
   *          description="contraseña del usuario",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *      @OA\Parameter(
   *          name="password_confirmation",
   *          description="confirmar contraseña del usuario",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *     security={ {"bearer": {}} },
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="400",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function store(UserRequest $request) {

    DB::beginTransaction();
    try {

      $user = new User();
      $user->fill($request->all());
      $user->password = Hash::make($request->password);
      $user->rol = User::ROL_CLIENTE;
      $user->save();

      DB::commit();

      return Utils::responseSuccess(MessageResponse::MESSAGE_CREATE_SUCCESS, CodesResponse::CODE_OK, $user);

    } catch (\Exception $exception) {

      DB::rollBack();

      return Utils::responseError($exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);

    }

  }

  /**
   * @OA\Get(
   *     path="/api/user/show/{id}",
   *     tags={"Usuarios"},
   *     summary="Mostrar el usuario especifico del id",
   *     security={ {"bearer": {}} },
   *   @OA\Parameter(
   *         name="id",
   *         in="path",
   *         description="User id",
   *         required=true,
   *         @OA\Schema(
   *             type="integer"
   *         )
   *   ),
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="default",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function show($id) {

    try {

      $user = User::findOrfail($id);

      return Utils::responseSuccess(MessageResponse::MESSAGE_QUERY_SUCCESS, CodesResponse::CODE_OK, $user);


    } catch (\Exception $exception) {
      return Utils::responseError(MessageResponse::MESSAGE_QUERY_EMPTY, CodesResponse::CODE_NOT_FOUND);
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    //
  }

  /**
   * @OA\Put(
   *     path="/api/user/update/{id}",
   *     tags={"Usuarios"},
   *     summary="Permite actualizar un usuario especifico buscandolo por su id",
   *     security={ {"bearer": {}} },
   *   @OA\Parameter(
   *         name="id",
   *         in="path",
   *         description="User id",
   *         required=true,
   *         @OA\Schema(
   *             type="integer"
   *         )
   *   ),
   *    @OA\Parameter(
   *          name="nickname",
   *          description="Nombre de usuario Ejeplo pepito03",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *   @OA\Parameter(
   *          name="nombre",
   *          description="Nombre y apelidos del usuario",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *     @OA\Parameter(
   *          name="documento",
   *          description="Número del documento de identidad del usuario (debe ser único)",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *     @OA\Parameter(
   *          name="telefono",
   *          description="Número telefonico de contacto",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *      @OA\Parameter(
   *          name="email",
   *          description="Correo electronico (debe ser único)",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *      @OA\Parameter(
   *          name="estado",
   *          description="Estado de los usuaurios Activo o Vetado",
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="default",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function update(UserRequest $request, $id) {
    DB::beginTransaction();
    try {

      $user = User::findOrfail($id);
      $user->fill($request->all());
      $user->save();

      DB::commit();

      return Utils::responseSuccess(MessageResponse::MESSAGE_UPDATE_SUCCESS, CodesResponse::CODE_OK, $user);

    } catch (\Exception $exception) {

      DB::rollBack();

      return Utils::responseError($exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);

    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id) {
    //
  }
}
