<?php

namespace App\Http\Controllers\API;

use App\Globals\CodesResponse;
use App\Globals\MessageResponse;
use App\Globals\Utils;
use App\Http\Requests\PeliculaRequest;
use App\Pelicula;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

class PeliculaController extends Controller {

  /**
   * @OA\Get(
   *     path="/api/peliculas",
   *     tags={"Peliculas"},
   *     summary="Mostrar todos las peliculas del sistema",
   *   description= "las imagenes de las portadas se encuentran en la carpeta imagenes/portada.png",
   *     security={ {"bearer": {}} },
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="default",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function index() {

    $peliculas = Pelicula::all();

    return Utils::responseSuccess(MessageResponse::MESSAGE_QUERY_SUCCESS, CodesResponse::CODE_OK, $peliculas);

  }


  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create() {
    //
  }

  /**
   * @OA\Post(
   *     path="/api/pelicula/store",
   *     tags={"Peliculas"},
   *     summary="Metodo que se encarga de registrar nuevas peliculas en el sistema",
   *    @OA\Parameter(
   *          name="portada",
   *          description="Imagen de portada de la pelicula (base64 -> data:image/jpeg;base64,/9j/4AAQS...)",
   *          required=true,
   *          in="header",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *    @OA\Parameter(
   *          name="nombre",
   *          description="Nombre de la Pelicula",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *     @OA\Parameter(
   *          name="descripcion",
   *          description="Breve descripcion de lo que trata la pelicula",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *     @OA\Parameter(
   *          name="genero",
   *          description="genero al que pertenerce la pelicula",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *    @OA\Parameter(
   *          name="cartelera",
   *          description="valor aceptado es 'X' o dejar vacio ",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),
   *     @OA\Parameter(
   *          name="fecha_lanzamiento",
   *          description="Fecha de lanzamiento de la pelicula",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="string"
   *          )
   *      ),@OA\Parameter(
   *          name="costo",
   *          description="Costo del alquiler de la pelicula",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="integer"
   *          )
   *      ),@OA\Parameter(
   *          name="cantidad",
   *          description="Numero de unidades disponibles de esta pelicula",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="integer"
   *          )
   *      ),
   *     security={ {"bearer": {}} },
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="400",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function store(PeliculaRequest $request) {

    DB::beginTransaction();
    try {

      $pelicula = new Pelicula();
      $pelicula->fill($request->all());

      if (!empty($request->portada)) {

        $uri = $request->portada;
        $img = explode(',', $uri);
        $ini =substr($img[0], 11);
        $type = explode(';', $ini);

        $uuid = Carbon::now()->format('YmdHis') . '.'.$type[0];
        $imagen = 'imagenes/' . $uuid;

        file_put_contents($imagen, base64_decode($img[1]));
      }
      $pelicula->portada = $uuid;

      $pelicula->save();

      DB::commit();

      return Utils::responseSuccess(MessageResponse::MESSAGE_CREATE_SUCCESS, CodesResponse::CODE_OK, $pelicula);

    } catch (\Exception $exception) {

      DB::rollBack();

      return Utils::responseError($exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);

    }


  }

  /**
   * @OA\Get(
   *     path="/api/pelicula/show/{id}",
   *     tags={"Peliculas"},
   *     summary="Mostrar la pelicula especifica del id",
   *     security={ {"bearer": {}} },
   *   @OA\Parameter(
   *         name="id",
   *         in="path",
   *         description="Pelicula id",
   *         required=true,
   *         @OA\Schema(
   *             type="integer"
   *         )
   *   ),
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="default",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function show($id) {
    try {

      $pelicula = Pelicula::findOrfail($id);

      return Utils::responseSuccess(MessageResponse::MESSAGE_QUERY_SUCCESS, CodesResponse::CODE_OK, $pelicula);


    } catch (\Exception $exception) {
      return Utils::responseError(MessageResponse::MESSAGE_QUERY_EMPTY, CodesResponse::CODE_NOT_FOUND);
    }
  }


  /**
   * @OA\Get(
   *     path="/api/pelicula/peliculasfiltro",
   *     tags={"Peliculas"},
   *     summary="Retorna todas las peliculs que coincidan con los campos",
   *     description="se puede ealizar busquedas por el nombre o por el genero o por ambas",
   *     security={ {"bearer": {}} },
   *   @OA\Parameter(
   *         name="nombre",
   *         in="path",
   *         description="Nombre de la pelicula a buscar",
   *         required=true,
   *         @OA\Schema(
   *             type="integer"
   *         )
   *   ),
   *   @OA\Parameter(
   *         name="genero",
   *         in="path",
   *         description="traer todas la peliculas del genero deseado",
   *         required=true,
   *         @OA\Schema(
   *             type="integer"
   *         )
   *   ),
   *   @OA\Parameter(
   *         name="cartelera",
   *         in="path",
   *         description="traer todas la peliculas que se encuentren en cartelera ",
   *         required=true,
   *         @OA\Schema(
   *             type="integer"
   *         )
   *   ),
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="default",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function peliculasFiltro(Request $request) {

    $query = [];

    if(!empty( $request->nombre )){
      array_push($query,['nombre',"like", "%".$request->nombre."%"]);
    }

    if(!empty( $request->genero )){
      array_push($query,['genero',"=", $request->genero]);
    }

    if(!empty( $request->cartelera )){
      array_push($query,['cartelera',"=", "X"]);
    }

    if(!empty( $request->perPage )){
      $perPage = $request->perPage;
    }else{
      $perPage = 15;
    }

    $peliculas = Pelicula::query()
      ->where($query)
      ->orderBy("id","desc")
      ->paginate($perPage);

    return Utils::responseSuccess(
      ($peliculas->count()>0)?MessageResponse::MESSAGE_QUERY_SUCCESS : MessageResponse::MESSAGE_QUERY_EMPTY,
      CodesResponse::CODE_OK,
      $peliculas);

  }



  /**
   * Show the form for editing the specified resource.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function edit($id) {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function destroy($id) {
    //
  }
}
