<?php

namespace App\Http\Controllers\API;

use App\Alquiler;
use App\Globals\CodesResponse;
use App\Globals\MessageResponse;
use App\Globals\Utils;
use App\Pelicula;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class AlquilerController extends Controller {


  /**
   * @OA\Post(
   *     path="/api/reservar",
   *     tags={"Alquiler"},
   *     summary="Metodo que se encarga de realizar las reservas de las peliculas al usuario que inicio sesión",
   *     @OA\Parameter(
   *          name="pelicula_id",
   *          description="id de la pelicula a reservar",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="integer"
   *          )
   *      ),
   *     security={ {"bearer": {}} },
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="400",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function reservar(Request $request) {

    $user = Auth::user();

    //verificamos que el usuario no cuente con la pelicula reservada o alquilada
    $pelicula = DB::table('pelicula_user')
      ->where('pelicula_id', $request->pelicula_id)
      ->where('user_id', $user->id)
      ->whereIn('estado', [Alquiler::ESTADO_RESERVADA, Alquiler::ESTADO_ALQUILADA])
      ->first();

    //si la pelicula se encuentra en estado reservada o alquilada NO dejamos seguir
    if ($pelicula) {
      return Utils::responseError("Esta pelicula se encuentra actualmente reserva o alquida para este usuario",
        CodesResponse::CODE_NOT_FOUND,
        []);
    }

    //Verificamos la exisgencia de esta pelicula en las reservas y alquires
    $numeroPelis = Pelicula::withCount([
      "reservas" => function ($query) {
        $query->whereIn('estado', [Alquiler::ESTADO_RESERVADA, Alquiler::ESTADO_ALQUILADA]);
      },
    ])->find($request->pelicula_id);

    //validamos que almenos exista una pelicula disponible para alquilar
    if (intval($numeroPelis->cantidad) - $numeroPelis->reservas_count == 0) {
      return Utils::responseSuccess("No existen unidades disponibles de esta Pelicula", CodesResponse::CODE_OK, []);
    }

    //realizamos la reserva de la pelicula
    $user->peliculas()->attach($request->pelicula_id, ['fecha_reserva' => Carbon::now()]);

    return Utils::responseSuccess("La Reserva fue realizada con Exito", CodesResponse::CODE_OK, []);

  }

  /**
   * @OA\Post(
   *     path="/api/cancelarreserva",
   *     tags={"Alquiler"},
   *     summary="Metodo que se encarga de cancelar una reserva el Unico que puede cancelar reservas de otros suarios es el Administrador",
   *     @OA\Parameter(
   *          name="user_id",
   *          description="el id del usuario que se le cancerara la reserva (SOLO EL ADMIN PUEDE HACER ESTO)",
   *          in="query",
   *          @OA\Schema(
   *              type="integer"
   *          )
   *      ),
   *     @OA\Parameter(
   *          name="reserva_id",
   *          description="id de la reservar",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="integer"
   *          )
   *      ),
   *     security={ {"bearer": {}} },
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="400",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function cancelarReserva(Request $request) {

    $user = Auth::user();

    if (!empty($request->user_id)) {

      if ($user->rol == User::ROL_ADMIN) {

        $user_id = $request->user_id;

      } else {
        return Utils::responseError("No tienes permiso para hacer esto", CodesResponse::CODE_UNAUTHORIZED, []);
      }

    } else {
      $user_id = $user->id;
    }

    $reserva = Alquiler::query()
      ->where("id", $request->reserva_id)
      ->where("user_id", $user_id)
      ->update(["estado"=> Alquiler::ESTADO_RESERVA_CANCELADA]);

    if($reserva){
      return Utils::responseSuccess("La Reserva se Cancelo con Exito", CodesResponse::CODE_OK, []);
    }else{
      return Utils::responseError("Ninguna reserva fue cancelada", CodesResponse::CODE_NOT_FOUND, []);
    }


  }

  /**
   * @OA\Get(
   *     path="/api/misreservas",
   *     tags={"Alquiler"},
   *     summary="Metodo que se encarga de entregar todas las reservar realizadas",
   *     security={ {"bearer": {}} },
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="400",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function misreservas() {

    $user = Auth::user();

    $peliculas = Alquiler::with("pelicula")
      ->where('user_id', $user->id)
      ->where('estado', 'Reservada')
      ->get();

    return Utils::responseSuccess(($peliculas->count() > 0) ? MessageResponse::MESSAGE_QUERY_SUCCESS : MessageResponse::MESSAGE_QUERY_EMPTY,
      CodesResponse::CODE_OK,
      $peliculas);

  }


  /**
   * @OA\Get(
   *     path="/api/reservas",
   *     tags={"Alquiler"},
   *     summary="Metodo que se encarga de entregar todas las reservar realizadas",
   *     security={ {"bearer": {}} },
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="400",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function reservas() {

    $peliculas = Alquiler::with("pelicula","user")
      ->where('estado', 'Reservada')
      ->get();

    return Utils::responseSuccess(($peliculas->count() > 0) ? MessageResponse::MESSAGE_QUERY_SUCCESS : MessageResponse::MESSAGE_QUERY_EMPTY,
      CodesResponse::CODE_OK,
      $peliculas);

  }

  /**
   * @OA\Post(
   *     path="/api/alquilar",
   *     tags={"Alquiler"},
   *     summary="Metodo que se encarga de alquilar una pelicula",
   *     @OA\Parameter(
   *          name="reserva_id",
   *          description="id de la reservar",
   *          required=true,
   *          in="query",
   *          @OA\Schema(
   *              type="integer"
   *          )
   *      ),
   *     security={ {"bearer": {}} },
   *     @OA\Response(
   *         response=200,
   *         description="Mostrar todos los usuarios."
   *     ),
   *     @OA\Response(
   *         response="400",
   *         description="Ha ocurrido un error."
   *     )
   * )
   */
  public function alquilar(Request $request) {

    $user = Auth::user();

    if ($user->rol != User::ROL_ADMIN) {

      return Utils::responseError("No tienes permiso para hacer esto", CodesResponse::CODE_UNAUTHORIZED, []);

    }

    DB::beginTransaction();
    try {

      $pelicula = DB::table('pelicula_user')
        ->where('id', $request->reserva_id)
        ->update(["fecha_alquiler" => Carbon::now(), "estado" => Alquiler::ESTADO_ALQUILADA]);

      DB::commit();

      return Utils::responseSuccess("La Pelicula se alquilo con exito", CodesResponse::CODE_OK, []);

    } catch (\Exception $exception) {

      DB::rollBack();

      return Utils::responseError($exception->getMessage(), CodesResponse::CODE_BAD_REQUEST);

    }
  }


}
