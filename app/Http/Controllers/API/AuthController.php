<?php

namespace App\Http\Controllers\API;

use App\Globals\CodesResponse;
use App\Globals\CryptoJs;
use App\Globals\MessageResponse;
use App\Globals\Utils;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller {

  public function __construct() {
    $this->middleware('jwt', ['except' => ['login']]);
  }

  /**
   * Get a JWT via given credentials.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function login() {
    $credentials = request(['email', 'password']);
    if (!$token = auth()->attempt($credentials)) {
      return Utils::responseError("Usuario o contraseña incorrectos", CodesResponse::CODE_UNAUTHORIZED);
    }
    return $this->respondWithToken($token);
  }

  /**
   * Get the authenticated User.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function me() {
    return response()->json(auth()->user());
  }

  public function payload() {
    return response()->json(auth()->payload());
  }

  /**
   * Log the user out (Invalidate the token).
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function logout() {
    auth()->logout();
    return response()->json(['message' => 'Successfully logged out']);
  }

  /**
   * Refresh a token.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function refresh() {
    return $this->respondWithToken(auth()->refresh());
  }

  /**
   * Get the token array structure.
   *
   * @param string $token
   *
   * @return \Illuminate\Http\JsonResponse
   */
  protected function respondWithToken($token) {

    $user = auth()->user();

    if($user->estado == User::ESTADO_ACTIVO){

      return response()->json([
        'access_token' => $token,
        'token_type' => 'bearer',
        'expires_in' => auth()->factory()->getTTL() * 60,
        'user' => [
          "nickname" => CryptoJs::cryptoJsAesEncrypt($token, $user->nickname),
          "nombre" => CryptoJs::cryptoJsAesEncrypt($token, $user->nombre),
          "rol" => CryptoJs::cryptoJsAesEncrypt($token, $user->rol)
        ],
      ]);

    }else{

      return Utils::responseError("Usuario Vetado no puedes Iniciar sesión", CodesResponse::CODE_UNAUTHORIZED);

    }


  }
}
