<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController {

  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  /**
   * @OA\Info(title="API Videos Pepito", version="1.0",
   *       description="Videos Pepito es una tienda de alquiler y reservas de peliculas",
   *   )
   * @OA\Server(url="http://videospepitoback.loc")
   *
   *  @OAS\SecurityScheme(
  *   securityScheme="bearerAuth",
  *   type="http",
  *    scheme="bearer"
  *    )
   *
   *
   */


  /**
   * @OA\Tag(
   *     name="Usuarios",
   *     description="Los usuarios del sistema pueden tener dos roles Administradores o Clientes",
   * )
   * @OA\Tag(
   *     name="Peliculas",
   *     description="Son todas las pelicuelas registradas en el sistema que pueden ser alquiladas o reservadas",
   * )
   * @OA\Tag(
   *     name="Alquiler",
   *     description="Aqui se realizan todas las acciones de alquiler y revervas de las peliculas",
   * )
   */


}
