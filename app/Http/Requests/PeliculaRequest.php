<?php

namespace App\Http\Requests;

use App\Globals\CodesResponse;
use App\Globals\KeysResponse;
use App\Globals\MethodsHttp;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class PeliculaRequest extends FormRequest {

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return TRUE;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {

    switch ($this->method()) {
      case MethodsHttp::METHOD_GET:
      case MethodsHttp::METHOD_DELETE:
        {
          return [];
        }
      case MethodsHttp::METHOD_POST:
        {
          return [
            "portada" => "required|string",
            "nombre" => "required|string",
            "descripcion" => "required|string",
            "genero" => "required|string",
            "fecha_lanzamiento" => "required|string",
            "costo" => "required|numeric",
            "cantidad" => "required|numeric",
          ];
        }
      case MethodsHttp::METHOD_PUT:
        {

          return [
            "portada" => "required|string",
            "nombre" => "required|string",
            "descripcion" => "required|string",
            "genero" => "required|string",
            "fecha_lanzamiento" => "required|string",
            "costo" => "required|numeric",
            "cantidad" => "required|numeric",
          ];
        }
      default:
        break;
    }
  }

  public function messages() {

    return [
      'portada.required' => 'La imagen de portada es obligatoria',
      'required' => ':attribute es un campo requerido.',
    ];
  }

  public function attributes() {

    return [
      "nombre"  => "El nombre de la pelicula",
      "decripcion" => "La decripcion de la pelicula",
      "genero" => "El genero al que pertenece la pelicula",
      "fecha_lanzamiento" => "La fecha de Lanzamineto",
      "costo" => "El costo de alquiler",
      "cantidad" => "La cantidad del inventario",
    ];
  }

  protected function failedValidation(Validator $validator) {

    throw new HttpResponseException(response()->json([
      KeysResponse::KEY_STATUS => KeysResponse::STATUS_ERROR,
      KeysResponse::KEY_DATA => $validator->errors()
    ],
      CodesResponse::CODE_FORM_INVALIDATE));
  }


}
