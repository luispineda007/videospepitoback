<?php

namespace App\Http\Requests;


use App\Globals\CodesResponse;
use App\Globals\KeysResponse;
use App\Globals\MethodsHttp;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;


class UserRequest extends FormRequest {

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return TRUE;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {

    switch ($this->method()) {
      case MethodsHttp::METHOD_GET:
      case MethodsHttp::METHOD_DELETE:
        {
          return [];
        }
      case MethodsHttp::METHOD_POST:
        {
          return [
            "nombre" => "required",
            "documento" => "required|numeric|unique:users,documento",
            "telefono" => "required|nullable|numeric",
            "email" => "unique:users,email",
            "password" => "required|string|min:6|confirmed"
          ];
        }
      case MethodsHttp::METHOD_PUT:
        {

          return [
            "nombre" => "required",
            "documento" => "required|numeric|unique:users,documento,{$this->id}",
            "telefono" => "required|nullable|numeric",
            "email" => "unique:users,email,{$this->id}",
          ];
        }
      default:
        break;
    }
  }

  public function messages() {

    return [
      'required' => 'El :attribute es un campo requerido.',
    ];
  }

  public function attributes() {

    return [
      "name"  => "nombre del usuario",
      "email" => "correo electronico",
      "telefono" => "numero de telefono o celular",
    ];
  }

  protected function failedValidation(Validator $validator) {

    throw new HttpResponseException(response()->json([
      KeysResponse::KEY_STATUS => KeysResponse::STATUS_ERROR,
      KeysResponse::KEY_DATA => $validator->errors()
    ],
      CodesResponse::CODE_FORM_INVALIDATE));
  }




}
