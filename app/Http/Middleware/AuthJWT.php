<?php

namespace App\Http\Middleware;

use App\Globals\CodesResponse;
use App\Globals\KeysResponse;
use App\Globals\Utils;
use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\JWTAuth;

class AuthJWT
{
  /**
   * The JWT Authenticator.
   *
   * @var \Tymon\JWTAuth\JWTAuth
   */
  protected $auth;

  /**
   * Create a new BaseMiddleware instance.
   *
   * @param  \Tymon\JWTAuth\JWTAuth  $auth
   *
   * @return void
   */
  public function __construct(JWTAuth $auth)
  {
    $this->auth = $auth;
  }

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \Closure $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    try {
      if (! $this->auth->parser()->setRequest($request)->hasToken()) {

        return Utils::responseError('No tiene token.' ,CodesResponse::CODE_UNAUTHORIZED);

      }
      if (! $this->auth->parseToken()->authenticate()) {

        return Utils::responseError('Usuario no encontrado.' ,CodesResponse::CODE_UNAUTHORIZED);

      }
    }
    catch (TokenExpiredException $exception){

      return Utils::responseError($exception->getMessage() ,CodesResponse::CODE_UNAUTHORIZED);

    }
    catch (JWTException $e) {

      return Utils::responseError('Token invalido ' ,CodesResponse::CODE_UNAUTHORIZED);

    }

    return $next($request);
  }
}
