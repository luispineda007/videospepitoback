<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Alquiler extends Model {

  const ESTADO_RESERVADA = 'Reservada';

  const ESTADO_ALQUILADA = 'Alquilada';

  const ESTADO_ENTREGADA = 'Entregada';

  const ESTADO_RESERVA_CANCELADA = 'Reserva-Cancelada';

  protected $table = 'pelicula_user';

  public function pelicula (){
    return $this->belongsTo(Pelicula::class);
  }

  public function user (){
    return $this->belongsTo(User::class);
  }

}
