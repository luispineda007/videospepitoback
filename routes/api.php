<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api','cors'])->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'auth','middleware' => 'cors'], function () {

  Route::post('login', 'API\AuthController@login');
  Route::post('logout', 'API\AuthController@logout');
  Route::post('refresh', 'API\AuthController@refresh');
  Route::post('me', 'API\AuthController@me');

});

Route::group(['middleware' => ['jwt','cors']], function () {


  Route::get('users', 'API\UserController@index');
  Route::get('usersall', 'API\UserController@users');

  Route::get('user/show/{id}', 'API\UserController@show');
  Route::put('user/update/{id}', 'API\UserController@update');
  Route::post('user/cambiarestado', 'API\UserController@cambiarEstado');

  Route::post('pelicula/store', 'API\PeliculaController@store');
  Route::put('pelicula/update/{id}', 'API\PeliculaController@update');

  Route::post('reservar', 'API\AlquilerController@reservar');
  Route::post('reserva/cancelar', 'API\AlquilerController@cancelarReserva');
  Route::get('misreservas', 'API\AlquilerController@misreservas');
  Route::get('reservas', 'API\AlquilerController@reservas');
  Route::post('alquilar', 'API\AlquilerController@alquilar');

});


Route::group(['middleware' => ['cors']], function () {

  Route::post('user/store', 'API\UserController@store');


  Route::get('peliculas', 'API\PeliculaController@index');
  Route::get('pelicula/show/{id}', 'API\PeliculaController@show');
  Route::get('pelicula/peliculasfiltro', 'API\PeliculaController@peliculasFiltro');





});